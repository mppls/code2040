# CODE2040
A repository for my 2015 CODE2040 API Challenge submission

These challenges involve sending HTTP POST requests to an endpiont on CODE2040's server, doing some computation, and sending the result back to a validation endpoint to check solutions.

1) Reverse a given string

2) Finding a given element within a given array

3) Remove elements form a given array with a given prefix

4) Computing new date and time from a given date and time and a given interval of seconds passed

My solutions are written in python.
