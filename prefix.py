import http.client
import ast

conn = http.client.HTTPConnection("challenge.code2040.org")

# Registration token
payload = "{\"token\":\"opnKrxZ1HV\"}"

conn.request("POST", "/api/prefix", payload)

res = conn.getresponse()
data = res.read().decode("utf-8")
#print(data)

# Turn dict string into dict object
d = ast.literal_eval(data)
d = d['result']

#print(d)

prefix = d["prefix"]
arr = d["array"]

print('prefix = ' + prefix)
print('array = ' + str(arr))

res = "["

for val in arr:
    if not prefix in val[:len(prefix)]:
        res += "\""+val+"\","

res = res[:len(res)-1] + "]"

print('res = ' + str(res))

payload = "{\"token\":\"opnKrxZ1HV\",\"array\":" + res + "}"

conn.request("POST", "/api/validateprefix", payload)

res = conn.getresponse()
data = res.read().decode("utf-8")

print()
print(data)
