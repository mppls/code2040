import http.client
import ast

conn = http.client.HTTPConnection("challenge.code2040.org")

# Registration token
payload = "{\"token\":\"opnKrxZ1HV\"}"

conn.request("POST", "/api/getstring", payload)

res = conn.getresponse()
data = res.read().decode("utf-8")

# Turn dict string into dict object
dictionary = ast.literal_eval(data)

string = dictionary['result']
print("string = " + string)

# Reverse string
string_reversed = string[::-1]
print("string_reversed = " + string_reversed)


payload = "{\"token\":\"opnKrxZ1HV\",\"string\":\"" + string_reversed + "\"}"

conn.request("POST", "/api/validatestring", payload)

res = conn.getresponse()
data = res.read().decode("utf-8")

print(data)
