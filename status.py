import http.client

conn = http.client.HTTPConnection("challenge.code2040.org")

# Registration token
payload = "{\"token\":\"opnKrxZ1HV\"}"

conn.request("POST", "/api/status", payload)

res = conn.getresponse()
data = res.read().decode("utf-8")

print(data)
