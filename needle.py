import http.client
import ast

conn = http.client.HTTPConnection("challenge.code2040.org")

# Registration token
payload = "{\"token\":\"opnKrxZ1HV\"}"

conn.request("POST", "/api/haystack", payload)

res = conn.getresponse()
data = res.read().decode("utf-8")
#print(data)

# Turn dict string into dict object
d = ast.literal_eval(data)
d = d['result']

#print(d)

haystack = d['haystack']
needle = d['needle']

print("haystack = " + str(haystack))
print("needle = " + needle)

pos = -1

for i in range(0, len(haystack)):
    if needle == haystack[i]:
        pos = i

print("pos = " + str(pos))

payload = "{\"token\":\"opnKrxZ1HV\",\"needle\":\"" + str(pos) + "\"}"

conn.request("POST", "/api/validateneedle", payload)

res = conn.getresponse()
data = res.read().decode("utf-8")

print()
print(data)
