import http.client

conn = http.client.HTTPConnection("challenge.code2040.org")

payload = "{\"email\":\"micahp@utexas.edu\", \"github\":\"https://github.com/micahp/CODE2040\"}"

conn.request("POST", "/api/register", payload)

res = conn.getresponse()
data = res.read()

print(data.decode("utf-8"))
