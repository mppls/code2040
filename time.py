import http.client
import datetime
import ast

conn = http.client.HTTPConnection("challenge.code2040.org")

# Registration token
payload = "{\"token\":\"opnKrxZ1HV\"}"

conn.request("POST", "/api/time", payload)

res  = conn.getresponse()
data = res.read().decode("utf-8")
#print(data)

# Turn dict string into dict object
d = ast.literal_eval(data)
d = d['result']

#print(d)

# Get passed in values
datestamp = d['datestamp']
interval  = d['interval' ]

print("datestamp = "  + datestamp    )
print("interval  = "  + str(interval))

# Seperate date and time from datetime
arr = datestamp.split('T')
datestring = arr[0]
timestring = arr[1]
timestring = timestring[:len(timestring)-1]

# Get new date by adding date and passed seconds
date = datetime.datetime.strptime(datestamp, "%Y-%m-%dT%H:%M:%S.%fZ")
newdate = date + datetime.timedelta(seconds=interval)
string = newdate.strftime("%Y-%m-%dT%H:%M:%S.%f")
# Format date for POST request
string = string[:-3] + "Z"
print("new date = " + string)

payload = "{\"token\":\"opnKrxZ1HV\",\"datestamp\":\"" + string + "\"}"

conn.request("POST", "/api/validatetime", payload)

res  = conn.getresponse()
data = res.read().decode("utf-8")

print()
print(data)
